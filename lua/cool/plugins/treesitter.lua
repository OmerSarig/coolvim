local function treesitter_config()
    local parsers_dir = require("cool.utils").download_dir
    vim.opt.runtimepath:prepend(parsers_dir)

    require("nvim-treesitter.configs").setup({
        auto_install = false,
        highlight = { enable = true },
        parser_install_dir = parsers_dir,
        incremental_selection = {
            enable = true,
            keymaps = {
                init_selection = "<C-n>",
                node_incremental = "<C-n>",
                scope_incremental = "<C-s>",
                node_decremental = "<C-r>",
            },
        },
    })

    vim.wo.foldmethod = "expr"
    vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
end

return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = function()
            local ts_update = require("nvim-treesitter.install").update({ with_sync = true })
            ts_update()
        end,
        config = treesitter_config,
    },
    {
        "romgrk/nvim-treesitter-context",
        dependencies = "nvim-treesitter/nvim-treesitter",
        opts = { enable = false },
    },
    {
        "nvim-treesitter/nvim-treesitter-textobjects",
        dependencies = "nvim-treesitter/nvim-treesitter",
        event = "VeryLazy",
        main = "nvim-treesitter.configs",
        opts = {
            textobjects = {
                select = {
                    enable = true,
                    lookahead = true,
                    keymaps = {
                        af = "@function.outer",
                        ["if"] = "@function.inner",
                        ac = "@class.outer",
                        ic = "@class.inner",
                        il = "@loop.inner",
                        al = "@loop.outer",
                    },
                },
            },
        },
    },
    { "HiPhish/rainbow-delimiters.nvim" },
    {
        "windwp/nvim-ts-autotag",
        dependencies = "nvim-treesitter/nvim-treesitter",
        config = true,
        ft = { "html", "javascript", "vue", "xml", "markdown" },
    },
    { "David-Kunz/treesitter-unit" },
    {
        "RRethy/nvim-treesitter-endwise",
        dependencies = "nvim-treesitter/nvim-treesitter",
        ft = { "python", "lua", "sh", "bash" },
        main = "nvim-treesitter.configs",
        opts = {
            endwise = { enable = true },
        },
    },
    {
        "danymat/neogen",
        dependencies = "nvim-treesitter/nvim-treesitter",
        opts = {
            snippet_engine = "luasnip",
            languages = {
                python = {
                    template = { annotation_convention = "google_docstrings" },
                },
            },
        },
    },
}
